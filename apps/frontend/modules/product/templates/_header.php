<ul class="sf_admin_actions">
<?php $product=$form->getObject();?>
<?php if($product->getId()){?>
	<li>  
	<?php echo link_to(
	  'Delete',
	  'product/delete?id='.$product->getId(),
	  array('method' => 'delete', 'confirm' => 'Are you sure?')
	) ?>
	</li>  
	<li class="sf_admin_action_list">
		<?php echo link_to("Cancel","product/view?id=".$product->getId())?>
	</li>  
    <li class="sf_admin_action_save_and_add">
      <input value="Save and add" name="_save_and_add" type="submit">
    </li>
<?php } else {?>
	<li class="sf_admin_action_list">
		<?php echo link_to("Cancel","home/index")?>
	</li>  
    <li class="sf_admin_action_save_and_add">
      <input value="Save and add" name="_save_and_add" type="submit">
    </li>
<?php } ?>
</ul>

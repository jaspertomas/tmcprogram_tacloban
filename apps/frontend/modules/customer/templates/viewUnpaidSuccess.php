<?php use_helper('I18N', 'Date'); ?>
<h1>Customer Unpaid Purchases: <?php echo $customer?> </h1>
Phone: <?php echo $customer->getPhone1()?>
<br><?php echo link_to("Edit","customer/edit?id=".$customer->getId()) ?>
<br><?php echo link_to("Print Billing Statement","customer/viewUnpaidPdf?id=".$customer->getId()) ?>

<br>
<br>
<table border=1>
  <tr>
    <td>Date</td>
    <td>Invoice</td>
    <td>Status</td>
    <td>Product</td>
    <td>Qty</td>
    <td>Price</td>
    <td>Discrate</td>
    <td>Discamt</td>
    <td>Total</td>
  </tr>
  <?php foreach($customer->getUnpaidInvoices("desc") as $invoice){?>
  <tr>
    <td><?php echo $invoice->getDate() ?></td>
    <td><?php echo link_to($invoice,"invoice/view?id=".$invoice->getId()) ?></td>
    <td><?php echo $invoice->getStatus() ?></td>
  </tr>
  <?php foreach($invoice->getInvoicedetail() as $detail){?>
  <tr>
  	<td></td>
  	<td></td>
  	<td></td>
    <td><?php echo $detail->getDescription() ?></td>
    <td><?php echo $detail->getQty() ?></td>
    <td><?php echo $detail->getPrice() ?></td>
    <td><?php echo $detail->getDiscrate() ?></td>
    <td><?php echo $detail->getDiscamt() ?></td>
    <td><?php echo $detail->getTotal() ?></td>
  </tr>
  <?php }?>
  <?php } ?>
</table>




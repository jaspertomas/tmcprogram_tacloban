<?php

require_once dirname(__FILE__).'/../lib/customerGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/customerGeneratorHelper.class.php';

/**
 * customer actions.
 *
 * @package    sf_sandbox
 * @subpackage customer
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class customerActions extends autoCustomerActions
{
  public function executeView(sfWebRequest $request)
  {
    $this->customer = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->invoice);
  }
  public function executeViewUnpaid(sfWebRequest $request)
  {
    $this->customer = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->invoice);
  }
  public function executeViewUnpaidPdf(sfWebRequest $request)
  {
    $this->executeViewUnpaid($request);

    $this->download=true;//$request->getParameter('download');
    $this->setLayout(false);
    $this->getResponse()->setContentType('pdf');
  }
  public function executeSearch(sfWebRequest $request)
  {
    $query=Doctrine_Query::create()
        ->from('Customer i')
      	->where('i.name like \'%'.trim($request->getParameter("searchstring")).'%\'')
        ->orderBy("i.name")
        ;

  	$results=$query->execute();
  	
  	if(count($results)==1)
  	{
            $this->customer=$results[0];
            $this->redirect("customer/view?id=".$this->customer->getId());
  	}
  	else
        {
            $this->customers=$results;
        }

  }
  public function executeNew(sfWebRequest $request)
  {
    $this->customer = new Customer();
    
    //set customer if param customer_name
    if($request->hasParameter("name"))
    {
      $this->customer->setName($request->getParameter("name"));
    }

    $this->form = $this->configuration->getForm($this->customer);
  }
  public function executeAdjust(sfWebRequest $request)
  {
    $requestparams=$request->getParameter("customer");
    $customer=Doctrine_Query::create()
        ->from('Customer c')
      	->where('c.id = '.$requestparams["id"])
      	->fetchOne();
    $customer->setAddress($requestparams["address"]);
    $customer->setPhone1($requestparams["phone1"]);
    $customer->setTinNo($requestparams["tin_no"]);
//    $customer->setRep($requestparams["rep"]);
//    $customer->setEmail($requestparams["email"]);
    $customer->setNotepad($requestparams["notepad"]);
    $customer->save();
    $this->redirect($request->getReferer());
  }
}

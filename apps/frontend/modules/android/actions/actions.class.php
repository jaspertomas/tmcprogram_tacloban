<?php

/**
 * android actions.
 *
 * @package    sf_sandbox
 * @subpackage android
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class androidActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $products= Doctrine_Query::create()
        ->from('Product p')
      	->execute()->getData();
      	
  	$array=array();
  	foreach($products as $product)
  	{
  	  $array[]=array(
  	    "id"=>$product->getId()
  	    ,"name"=>$product->getName()
//  	    ,"description"=>$product->getDescription()
  	    ,"producttype_id"=>$product->getProducttypeId()
  	    ,"minsellprice"=>$product->getMinSellPrice()
  	    ,"maxsellprice"=>$product->getMaxSellPrice()
  	    ,"minbuyprice"=>$product->getMinBuyPrice()
  	    ,"maxbuyprice"=>$product->getMaxBuyPrice()
  	    );
  	}
  	echo json_encode($array);
  	die();
  }
}

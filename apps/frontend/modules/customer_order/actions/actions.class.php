<?php

require_once dirname(__FILE__).'/../lib/customer_orderGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/customer_orderGeneratorHelper.class.php';

/**
 * customer_order actions.
 *
 * @package    sf_sandbox
 * @subpackage customer_order
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class customer_orderActions extends autoCustomer_orderActions
{
  public function executeNew(sfWebRequest $request)
  {
    $this->customer_order = new CustomerOrder();
    $this->customer_order->setDate(MyDate::today());
       		
    $this->form = $this->configuration->getForm($this->customer_order);
  }
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $customer_order = $form->save();
        $customer_order->genCustomer();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $customer_order)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@customer_order_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect(array('sf_route' => 'customer_order_edit', 'sf_subject' => $customer_order));
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
}

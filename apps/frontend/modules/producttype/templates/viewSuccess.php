<?php use_helper('I18N', 'Date') ?>

<h1><?php echo "Product Type: ".$producttype->getName() ?></h1>

<?php echo link_to("Edit","producttype/edit?id=".$producttype->getId());?>
<br><?php echo link_to("Back to List","producttype/index");?>
<br><?php echo link_to("Print Inventory Sheet","producttype/viewPdf?id=".$producttype->getId());?>
<br><?php echo link_to("View Products Below Quota","producttype/belowQuota?id=".$producttype->getId());?>

<table border=1>
  <tr>
    <td>Id</td>
    <td><input id="product_ids_header_checkbox" onclick="checkAllProductIds();" type="checkbox"></td>
    <td>Product</td>
    <td>Description</td>
    <td>Max Sale</td>
    <!--td></td-->
    <td>Min Sale</td>
    <!--td></td-->
    <td>Max Vendor</td>
    <!--td></td-->
    <td>Min Vendor</td>
    <td>Qty</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Price</td>
    <!--td></td-->
    <td>Price</td>
    <!--td></td-->
    <td>Price</td>
    <!--td></td-->
    <td>Price</td>
    <!--td></td-->
    <td></td>
  </tr>
  <?php
//      $maxtotal=0;
//  	if($producttype->getId()==1)$products=array();
//  	else $products=$producttype->getProducts();
        foreach($products as $product){@$stock=$stockarray[$product->getId()];
  /* 
		//count number of invoicedetails * qty sold since a certain date
		$details = Doctrine_Query::create()
      ->from('Invoicedetail pd, pd.Invoice i')
      ->where('pd.product_id = '.$product->getId())
//      ->andWhere('i.date >= "2010-09-28"')
      ->execute();
      $max=0;
foreach($details as $detail){$max+=$detail->getQty();}

	$maxtotal+=$max;*/
  	
 	?>
  <tr>
    <td><?php echo $product->getId() ?></td>
    <td><input type=checkbox name="product_ids[]" value="<?php echo $product->getId()?>" /></td>
    <td><?php echo link_to($product->getName(),"product/view?id=".$product->getId()) ?></td>
    <td><?php echo $product->getDescription() ?></td>
    <td align=right><?php echo MyDecimal::format($product->getMaxsellprice()==""?0:$product->getMaxsellprice()) ?></td>
    <!--td><input name="maxsellprices[<?php echo $product->getId()?>]" size=1 /></td-->
    <td align=right><?php echo MyDecimal::format($product->getMinsellprice()==""?0:$product->getMinsellprice()) ?></td>
    <!--td><input name="minsellprices[<?php echo $product->getId()?>]" size=1 /></td-->
    <td align=right><font color=gray><?php echo MyDecimal::format($product->getMaxbuyprice()==""?0:$product->getMaxbuyprice()) ?></font></td>
    <!--td><input name="maxbuyprices[<?php echo $product->getId()?>]" size=1 /></td-->
    <td align=right><font color=gray><?php echo MyDecimal::format($product->getMinbuyprice()==""?0:$product->getMinbuyprice()) ?></fontx></td>
    <!--td><input name="minbuyprices[<?php echo $product->getId()?>]" size=1 /></td-->
    <td align=right><?php if($stock)echo link_to($stock->getCurrentQty(),"product/inventory?id=".$product->getId());?></td>
    <td><?php echo link_to("Edit","product/edit?id=".$product->getId()) ?></td>
    <td><?php echo link_to(
  'Delete',
  'product/delete?id='.$product->getId(),
  array('method' => 'delete', 'confirm' => 'Are you sure?')
) ?></td>
    <td><center><bold><?php //echo $max?$max:"" ?></bold></center></td>
  </tr>
  <?php }?>
</table>



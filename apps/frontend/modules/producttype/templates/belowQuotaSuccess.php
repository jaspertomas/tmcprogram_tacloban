<h1>Products that Need Reordering: <?php if($producttype)echo $producttype->getName();else echo "ALL Products"?></h1>
<?php 
if($producttype)echo link_to("Print","producttype/belowQuotaPdf?id=".$producttype->getId());
else echo link_to("Print","producttype/belowQuotaPdf");
?>
<br>
<br>
<table border=1>
<tr>
	<td>Id</td>
	<td>Name</td>
	<td>Description</td>
	<td>Quota</td>
	<td>Remaining Stock</td>
	<td>Edit</td>
	<!--
	<td>Price List</td>
	<td>Transactions</td>
	-->
</tr>
<?php foreach($stocks as $stock){$product=$stock->getProduct();?>
<tr>
	<td><?php echo $product->getId()?></td>
	<td><?php echo $product->getName()?></td>
	<td><?php echo $product->getDescription()?></td>
	<td><?php echo $product->getQuota()?></td>
	<td align=right><?php if($stock)echo link_to($stock->getCurrentQty(),"product/inventory?id=".$product->getId());?></td>
	<td><?php echo link_to("Edit","product/edit?id=".$product->getId());?></td>
	<!--
	<td><?php echo link_to("Price List","producttype/view?id=".$product->getProducttypeId());?></td>
	<td><?php echo link_to("Transactions","product/view?id=".$product->getId());?></td>
	-->
</tr>
<?php	} ?>
</table>


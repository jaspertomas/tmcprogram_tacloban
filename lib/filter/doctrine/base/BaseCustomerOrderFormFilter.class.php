<?php

/**
 * CustomerOrder filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCustomerOrderFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'customer_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => true)),
      'customer_name'  => new sfWidgetFormFilterInput(),
      'customer_phone' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'description'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'status'         => new sfWidgetFormChoice(array('choices' => array('' => '', 'Pending' => 'Pending', 'Ordered' => 'Ordered', 'Arrived' => 'Arrived', 'Sold' => 'Sold'))),
      'is_cancelled'   => new sfWidgetFormChoice(array('choices' => array('' => '', 'False' => 'False', 'True' => 'True'))),
    ));

    $this->setValidators(array(
      'customer_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Customer'), 'column' => 'id')),
      'customer_name'  => new sfValidatorPass(array('required' => false)),
      'customer_phone' => new sfValidatorPass(array('required' => false)),
      'date'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'description'    => new sfValidatorPass(array('required' => false)),
      'status'         => new sfValidatorChoice(array('required' => false, 'choices' => array('Pending' => 'Pending', 'Ordered' => 'Ordered', 'Arrived' => 'Arrived', 'Sold' => 'Sold'))),
      'is_cancelled'   => new sfValidatorChoice(array('required' => false, 'choices' => array('False' => 'False', 'True' => 'True'))),
    ));

    $this->widgetSchema->setNameFormat('customer_order_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CustomerOrder';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'customer_id'    => 'ForeignKey',
      'customer_name'  => 'Text',
      'customer_phone' => 'Text',
      'date'           => 'Date',
      'description'    => 'Text',
      'status'         => 'Enum',
      'is_cancelled'   => 'Enum',
    );
  }
}

<?php

/**
 * CustomerOrder form base class.
 *
 * @method CustomerOrder getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCustomerOrderForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'customer_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => false)),
      'customer_name'  => new sfWidgetFormInputText(),
      'customer_phone' => new sfWidgetFormInputText(),
      'date'           => new sfWidgetFormDate(),
      'description'    => new sfWidgetFormTextarea(),
      'status'         => new sfWidgetFormChoice(array('choices' => array('Pending' => 'Pending', 'Ordered' => 'Ordered', 'Arrived' => 'Arrived', 'Sold' => 'Sold'))),
      'is_cancelled'   => new sfWidgetFormChoice(array('choices' => array('False' => 'False', 'True' => 'True'))),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'customer_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'))),
      'customer_name'  => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'customer_phone' => new sfValidatorString(array('max_length' => 50)),
      'date'           => new sfValidatorDate(),
      'description'    => new sfValidatorString(),
      'status'         => new sfValidatorChoice(array('choices' => array(0 => 'Pending', 1 => 'Ordered', 2 => 'Arrived', 3 => 'Sold'))),
      'is_cancelled'   => new sfValidatorChoice(array('choices' => array(0 => 'False', 1 => 'True'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('customer_order[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CustomerOrder';
  }

}

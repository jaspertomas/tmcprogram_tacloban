
--
-- Table structure for table `priceitem`
--

CREATE TABLE IF NOT EXISTS `priceitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pricelist_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pricelist_id` (`pricelist_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `producttype_schema`
--

CREATE TABLE IF NOT EXISTS `producttype_schema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `product_name_format` varchar(100) DEFAULT NULL,
  `product_description_format` varchar(100) DEFAULT NULL,
  `barcode_format` varchar(100) DEFAULT NULL,
  `barcode_format_2` varchar(100) DEFAULT NULL,
  `barcode_format_3` varchar(100) DEFAULT NULL,
  `barcode_template` enum('Simple 1 Column','Simple 2 Column','Simple 2 Column Green and White','Standard 3 Column') NOT NULL DEFAULT 'Simple 1 Column',
  `max_buy_formula` varchar(100) DEFAULT NULL,
  `min_buy_formula` varchar(100) DEFAULT NULL,
  `max_sell_formula` varchar(100) DEFAULT NULL,
  `min_sell_formula` varchar(100) DEFAULT NULL,
  `base_formula` varchar(100) DEFAULT NULL,
  `speccount` int(11) NOT NULL DEFAULT '0',
  `spec1` varchar(30) DEFAULT NULL,
  `spec2` varchar(30) DEFAULT NULL,
  `spec3` varchar(30) DEFAULT NULL,
  `spec4` varchar(30) DEFAULT NULL,
  `spec5` varchar(30) DEFAULT NULL,
  `spec6` varchar(30) DEFAULT NULL,
  `spec7` varchar(30) DEFAULT NULL,
  `spec8` varchar(30) DEFAULT NULL,
  `spec9` varchar(30) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `producttype_schema`
--

INSERT INTO `producttype_schema` (`id`, `name`, `description`, `product_name_format`, `product_description_format`, `barcode_format`, `barcode_format_2`, `barcode_format_3`, `barcode_template`, `max_buy_formula`, `min_buy_formula`, `max_sell_formula`, `min_sell_formula`, `base_formula`, `speccount`, `spec1`, `spec2`, `spec3`, `spec4`, `spec5`, `spec6`, `spec7`, `spec8`, `spec9`, `notes`) VALUES
(1, 'None', NULL, NULL, NULL, NULL, NULL, NULL, 'Simple 1 Column', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Era 1', '', '', '', '', '', '', 'Simple 2 Column Green and White', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', ''),
(3, 'Era 2', '', '', '', 'fraction+x+fraction2+ +inch', 'mm+x+mm2+ +''mm', 'color', 'Simple 2 Column Green and White', '', '', '', '', '', 7, 'fraction', 'decimal', 'mm', 'fraction2', 'decimal2', 'mm2', 'color', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `profitdetail`
--

CREATE TABLE IF NOT EXISTS `profitdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `invoicedetail_id` int(11) NOT NULL,
  `purchasedetail_id` int(11) NOT NULL,
  `invoicedate` date NOT NULL,
  `purchasedate` date NOT NULL,
  `buyprice` decimal(10,2) NOT NULL,
  `sellprice` decimal(10,2) NOT NULL,
  `profitperunit` decimal(10,2) NOT NULL,
  `profitrate` decimal(10,2) NOT NULL,
  `qty` decimal(10,2) NOT NULL,
  `profit` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `purchase_id` (`purchase_id`),
  KEY `invoicedetail_id` (`invoicedetail_id`),
  KEY `purchasedetail_id` (`purchasedetail_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `profitdetail`
--
-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE IF NOT EXISTS `voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` varchar(8) NOT NULL,
  `account_id` int(11) NOT NULL,
  `voucher_type_id` int(11) NOT NULL,
  `voucher_allocation_id` int(11) NOT NULL,
  `payee` varchar(100) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `particulars` tinytext NOT NULL,
  `check_no` varchar(30) DEFAULT NULL,
  `check_status` enum('Pending','Cleared','On Hold','Cancelled') DEFAULT 'Pending',
  `notes` tinytext,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `voucher_type_id` (`voucher_type_id`),
  KEY `voucher_allocation_id` (`voucher_allocation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `voucher_allocation`
--

CREATE TABLE IF NOT EXISTS `voucher_allocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `voucher_allocation`
--

INSERT INTO `voucher_allocation` (`id`, `name`) VALUES
(1, 'Tradewind'),
(2, 'Seawings'),
(3, 'Malabon');

-- --------------------------------------------------------

--
-- Table structure for table `voucher_type`
--

CREATE TABLE IF NOT EXISTS `voucher_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `voucher_type`
--

INSERT INTO `voucher_type` (`id`, `name`) VALUES
(1, 'Petty Cash'),
(2, 'Cheque'),
(3, 'Bank Transfer'),
(4, 'Personal'),
(5, 'Other');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `priceitem`
--
ALTER TABLE `priceitem`
  ADD CONSTRAINT `priceitem_ibfk_1` FOREIGN KEY (`pricelist_id`) REFERENCES `pricelist` (`id`),
  ADD CONSTRAINT `priceitem_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Constraints for table `profitdetail`
--
ALTER TABLE `profitdetail`
  ADD CONSTRAINT `profitdetail_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `profitdetail_ibfk_2` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `profitdetail_ibfk_3` FOREIGN KEY (`purchase_id`) REFERENCES `purchase` (`id`),
  ADD CONSTRAINT `profitdetail_ibfk_4` FOREIGN KEY (`invoicedetail_id`) REFERENCES `invoicedetail` (`id`),
  ADD CONSTRAINT `profitdetail_ibfk_5` FOREIGN KEY (`purchasedetail_id`) REFERENCES `purchasedetail` (`id`);

--
-- Constraints for table `voucher`
--
ALTER TABLE `voucher`
  ADD CONSTRAINT `voucher_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  ADD CONSTRAINT `voucher_ibfk_2` FOREIGN KEY (`voucher_type_id`) REFERENCES `voucher_type` (`id`),
  ADD CONSTRAINT `voucher_ibfk_3` FOREIGN KEY (`voucher_allocation_id`) REFERENCES `voucher_allocation` (`id`);




INSERT INTO `account` (`id`, `code`, `name`, `account_type_id`, `account_category_id`, `is_special`, `currentqty`, `date`) VALUES
(22, 'Travel', 'Travel', 5, NULL, NULL, NULL, NULL),
(23, 'Insurance', 'Insurance', 5, NULL, NULL, NULL, NULL),
(24, 'Medical', 'Medical', 5, NULL, NULL, NULL, NULL),
(25, 'Construction', 'Construction', 5, NULL, NULL, NULL, NULL),
(26, 'Taxes and Fees', 'Taxes and Fees', 5, NULL, NULL, NULL, NULL),
(30, 'Utils-Water', 'Utils-Water', 5, NULL, NULL, NULL, NULL),
(31, 'Utils-Landline', 'Utils-Landline', 5, NULL, NULL, NULL, NULL),
(32, 'Utils-Mobile', 'Utils-Mobile', 5, NULL, NULL, NULL, NULL),
(33, 'Utils-Electricity', 'Utils-Electricity', 5, NULL, NULL, NULL, NULL),
(34, 'Food', 'Food', 5, NULL, NULL, NULL, NULL),
(35, 'Miscellaneous', 'Miscellaneous', 5, NULL, NULL, NULL, NULL),
(36, 'Salary', 'Salary', 5, NULL, NULL, NULL, NULL);

INSERT INTO `account_category` (`id` ,`name` ,`code` ,`account_type_id` ,`parent_code`)VALUES (13 , 'Petty Cash Accounts', '', '5', NULL);
UPDATE account SET account_category_id =13 WHERE id >20;


-- --------more from 'adjustments'----------------------

-- now there is no more such thing as a cancelled stock entry. -----
-- stock entries are deleted when invoices are cancelled------------
delete from stockentryx where is_cancelled=1;
delete from quote where is_cancelled=1;

ALTER TABLE `stockentry` DROP `is_cancelled` ;
ALTER TABLE `quote` DROP `is_cancelled` ;

-- --------producttype------------

ALTER TABLE `producttype` 
 ADD  `spec9` varchar(30) DEFAULT NULL AFTER `priority`,
 ADD  `spec8` varchar(30) DEFAULT NULL AFTER `priority`,
 ADD  `spec7` varchar(30) DEFAULT NULL AFTER `priority`,
 ADD  `spec6` varchar(30) DEFAULT NULL AFTER `priority`,
 ADD  `spec5` varchar(30) DEFAULT NULL AFTER `priority`,
 ADD  `spec4` varchar(30) DEFAULT NULL AFTER `priority`,
 ADD  `spec3` varchar(30) DEFAULT NULL AFTER `priority`,
 ADD  `spec2` varchar(30) DEFAULT NULL AFTER `priority`,
 ADD  `spec1` varchar(30) DEFAULT NULL AFTER `priority`,
 ADD  `speccount` int(11) NOT NULL DEFAULT '0' AFTER `priority`,
 ADD  `base_formula` varchar(100) DEFAULT NULL AFTER `priority`,
 ADD  `min_sell_formula` varchar(100) DEFAULT NULL AFTER `priority`,
 ADD  `max_sell_formula` varchar(100) DEFAULT NULL AFTER `priority`,
 ADD  `min_buy_formula` varchar(100) DEFAULT NULL AFTER `priority`,
 ADD  `max_buy_formula` varchar(100) DEFAULT NULL AFTER `priority`,
 ADD  `barcode_format_3` varchar(100) DEFAULT NULL AFTER `priority`,
 ADD  `barcode_format_2` varchar(100) DEFAULT NULL AFTER `priority`,
 ADD  `barcode_format` varchar(100) DEFAULT NULL AFTER `priority`,
 ADD  `product_description_format` varchar(150) DEFAULT NULL AFTER `priority`,
 ADD  `product_name_format` varchar(100) DEFAULT NULL AFTER `priority`;


ALTER TABLE `producttype` ADD `producttype_schema_id` INT NOT NULL DEFAULT '1' AFTER `parent_id` ;
ALTER TABLE `producttype` ADD INDEX ( `producttype_schema_id` ) ;
ALTER TABLE `producttype` ADD FOREIGN KEY ( `producttype_schema_id` ) REFERENCES `producttype_schema` (
`id`
) ON DELETE RESTRICT ON UPDATE CASCADE ;
ALTER TABLE `producttype` ADD INDEX ( `parent_id` ) ;
ALTER TABLE `producttype` ADD FOREIGN KEY ( `parent_id` ) REFERENCES `producttype` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;


ALTER TABLE `producttype`
  DROP `category1`,
  DROP `category2`,
  DROP `category3`,
  DROP `category4`,
  DROP `category5`,
  DROP `category6`,
  DROP `category7`,
  DROP `category8`,
  DROP `category9`,
  DROP `category10`;
  
ALTER TABLE `producttype` drop status;
ALTER TABLE `producttype`
   add `status` enum('OK','Update Required') NOT NULL DEFAULT 'Update Required' after path;


-- ------invoicedetail and purchasedetail------------

ALTER TABLE `purchasedetail` ADD `remaining` DECIMAL(10,2) NULL DEFAULT '0';

ALTER TABLE `invoicedetail` 
 add `is_discounted` tinyint(4) DEFAULT '0',
 add `remaining` decimal(10,2) DEFAULT '0.00',
 add `is_profitcalculated` tinyint(4) DEFAULT '0',
 add `with_commission` decimal(10,2) NOT NULL DEFAULT '0.00',
 add `with_commission_total` decimal(10,2) NOT NULL DEFAULT '0.00',
 add `is_vat` tinyint(4) NOT NULL DEFAULT '0';


update purchasedetail set remaining = qty;
update invoicedetail set remaining = qty;

-- ------product------------

ALTER TABLE `product` DROP `category1` ,
DROP `category2` ,
DROP `category3` ,
DROP `category4` ,
DROP `category5` ,
DROP `category6` ,
DROP `category7` ,
DROP `category8` ,
DROP `category9` ,
DROP `category10` ;


ALTER TABLE `product` 
 add `baseprice` decimal(10,2) DEFAULT '0.00' after maxsellprice,
 add `code` varchar(20) DEFAULT NULL after description,
 add `spec1` varchar(30) DEFAULT NULL after code,
 add `spec2` varchar(30) DEFAULT NULL after spec1,
 add `spec3` varchar(30) DEFAULT NULL after spec2,
 add `spec4` varchar(30) DEFAULT NULL after spec3,
 add `spec5` varchar(30) DEFAULT NULL after spec4,
 add `spec6` varchar(30) DEFAULT NULL after spec5,
 add `spec7` varchar(30) DEFAULT NULL after spec6,
 add `spec8` varchar(30) DEFAULT NULL after spec7,
 add `spec9` varchar(30) DEFAULT NULL after spec8;
 
 ALTER TABLE `product` CHANGE `publish` `is_hidden` TINYINT( 4 ) not NULL DEFAULT '0' ;
 
-- foreign key with brand id
 ALTER TABLE `product` DROP FOREIGN KEY `product_ibfk_1` ; 

ALTER TABLE `product` CHANGE `autocalcsellprice` `autocalcsellprice` TINYINT( 4 ) NOT NULL DEFAULT '0',
CHANGE `autocalcbuyprice` `autocalcbuyprice` TINYINT( 4 ) NOT NULL DEFAULT '1';


-- customer-----------------------
ALTER TABLE `customer` 
 add `address2` varchar(100) DEFAULT NULL after address;
-- customer notes field is too short (60 chars). this will make it a lot longer. -----
ALTER TABLE `customer` CHANGE `notepad` `notepad` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
-- customer order-----------------------
ALTER TABLE `customer_order` CHANGE `customer_phone` `customer_phone` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;

-- settings-----------------------
INSERT INTO `settings` (
`id` ,
`name` ,
`value`
)
VALUES (
NULL , 'vat_product_id', '987'
);

INSERT INTO `settings` (
`id` ,
`name` ,
`value`
)
VALUES (
NULL , 'default_customer_id', '1'
);


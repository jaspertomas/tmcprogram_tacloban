ALTER TABLE `invoice` CHANGE `status` `status` ENUM( 'Pending', 'Paid', 'Cancelled', 'Overpaid' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'Pending';
